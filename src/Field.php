<?php

namespace Drupal\date_period;

use Drupal\date_period\Field\Quarter;
use Drupal\mixin\Caller;
use Drupal\mixin\Getter;
use Drupal\mixin\Traits\Hook;

/** Class Field */
class Field {
  use Hook;

  /** @return array */
  static function hook_field_formatter_info() {
    //$module = self::getType()->getModule();

    $items = [
      'quarter' => [
        'label' => t('Quarter'),
        'field types' => ['date', 'datestamp', 'datetime'],
        //'settings' => [
        //  'plural' => t('Quarters', [], ['context' => $module]),
        //  'singular' => t('Quarter', [], ['context' => $module]),
        //  'php' => 'months',
        //  'multiplier' => 3,
        //],
      ],
    ];

    return $items;
  }

  /** @return array */
  static function hook_token_info() {
    $tokens = [];
    $tokens['date']['quarter'] = [
      'name' => t('Quarter'),
      'description' => t('Quarter'),
    ];

    $items = compact('tokens');
    return $items;
  }

  /** @noinspection PhpUnusedParameterInspection
   * @param $type
   * @param $tokens
   * @param array $data
   * @param array $options
   *
   * @return array
   */
  static function hook_tokens($type, $tokens, array $data = [], array $options = []) {
    $replacements = [];

    if ('date' != $type) {
      return $replacements;
    }

    foreach ($tokens as $name => $original) {
      if ('quarter' == $name) {
        $replacements[$original] = Quarter::formatValue($data[$type]);
      }
    }

    return $replacements;
  }

  /** @noinspection PhpTooManyParametersInspection
   * @param $entity_type
   * @param $entity
   * @param $field
   * @param $instance
   * @param $langcode
   * @param $items
   * @param $display
   *
   * @return array
   */
  static function hook_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
    $element = ['#markup' => ''];

    if ('quarter' == $display['type']) {
      $element = Quarter::formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display);

    }

    return $element;
  }

  /**
   * @param $form
   * @param $context
   */
  static function hook_date_field_settings_form_alter(&$form, $context) {
    $form['period'] = [
      '#type' => 'select',
      '#title' => t('Period'),
      '#default_value' => static::getPeriod($context['field']),
      '#options' => static::getOptions(),
    ];
  }

  /**
   * @param $field
   *
   * @return mixed
   */
  protected static function getPeriod($field) {
    return Getter::create($field, ['settings', 'period'])->setBuilder(Caller::create(null, ''))->fetch(false);
  }

  /** @return array */
  protected static function getOptions() {
    $items = ['' => t('None'), 'quarter' => t('Quarter')];
    return $items;
  }

  /**
   * @param $element
   * @param $form_state
   * @param $context
   */
  static function hook_date_select_process_alter(&$element, &$form_state, $context) {
    $period = isset($element['#field']) ? static::getPeriod($element['#field']) : $element['#date_period'];

    if ('quarter' == $period) {
      Quarter::process_alter($element, $form_state, $context);
    }
  }

  /**
   * @param $element
   * @param $form_state
   * @param $input
   */
  static function hook_date_select_pre_validate_alter(&$element, &$form_state, &$input) {
    $period = isset($element['#field']) ? static::getPeriod($element['#field']) : $element['#date_period'];

    if ('quarter' == $period) {
      Quarter::validate_alter($element, $form_state, $input);
    }
  }

  /** @param array $type */
  static function hook_element_info_alter(&$type) {
    $type['date_select'] += ['#date_period' => '', '#allow_whole_year' => false];
  }

  /** @return array */
  protected static function getHookMap() {
    $class = get_called_class();
    $module = static::getType()->getModule();
    $hooks = [];
    $items = [
      'element_info_alter',
      'field_formatter_view',
      'field_formatter_info',
      'token_info',
      'tokens',
      'date_field_settings_form_alter',
      'date_select_process_alter',
      'date_select_pre_validate_alter',
    ];

    foreach ($items as $item) {
      $hooks[$class]['hook_' . $item] = $module . '_' . $item;
    }

    return $hooks;
  }

}
