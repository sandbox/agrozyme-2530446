<?php
namespace Drupal\date_period\Field;

use DateObject;
use Drupal\mixin\Caller;
use Drupal\mixin\Getter;
use Drupal\mixin\Traits\Object;

/** Class Quarter */
class Quarter {
  use Object;

  /** @noinspection PhpTooManyParametersInspection, PhpUnusedParameterInspection
   * @param $entity_type
   * @param $entity
   * @param $field
   * @param $instance
   * @param $langcode
   * @param $items
   * @param $display
   *
   * @return array
   */
  static function formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
    $element = [];

    foreach ($items as $delta => $item) {
      $element[$delta] = ['#markup' => static::formatValue($item['value'])];
    }

    return $element;
  }

  /**
   * @param $value
   *
   * @return string
   */
  static function formatValue($value) {
    $item = new DateObject($value);
    $date = $item->toArray();
    $month = intval($date['month']);
    $map = static::getMonthMap();
    $quarter = Getter::create($map, [$month])->setBuilder(Caller::create(null, 0))->fetch(false);
    $output = $date['year'];

    if (0 < $quarter) {
      $output .= ' Q' . $quarter;
    }

    return $output;
  }

  /** @return mixed */
  static function getMonthMap() {
    static $data = [];

    if (false == empty($data)) {
      return $data;
    }

    $month_per_quarter = 3;
    $quarter_per_year = 4;

    for ($quarter = 1; $quarter <= $quarter_per_year; $quarter++) {
      $index = ($quarter - 1) * $month_per_quarter;

      for ($offset = 1; $offset <= $month_per_quarter; $offset++) {
        $data[$index + $offset] = $quarter;
      }
    }

    return $data;
  }

  /**
   * @param $element
   * @param $form_state
   * @param $context
   */
  static function process_alter(&$element, &$form_state, $context) {
    foreach (static::getDisableFields() as $field) {
      $element[$field]['#type'] = 'value';
      $element[$field]['#value'] = 0;
    }

    static::month_alter($element);

    if (empty($form_state['input'])) {
      return;
    }

  }

  /** @return array */
  protected static function getDisableFields() {
    return ['day', 'hour', 'minute', 'second'];
  }

  /** @param array $element */
  static function month_alter(&$element) {
    $whole_year = (false == empty($element['#whole_year']));
    $required = (false == empty($element['#required']));
    $field = 'month';
    $title = t('Quarter');
    $options = static::getOptions($required, $whole_year);
    $month = &$element[$field];
    $month['#title'] = $title;
    $month['#options'] = $options;
    static::setQuarterDefaultValue($element);

    if ('within' == $element['#date_label_position']) {
      $month['#options'] = ['' => '-' . $title] + $month['#options'];
    }
  }

  /**
   * @param bool $required
   * @param bool $whole_year
   *
   * @return array
   */
  static function getOptions($required = false, $whole_year = false) {
    static $items;

    $callback = function () {
      $data = [];
      $month_per_quarter = 3;
      $quarter_per_year = 4;

      for ($quarter = 1; $quarter <= $quarter_per_year; $quarter++) {
        $index = ($quarter - 1) * $month_per_quarter;
        $data[$index + 1] = $quarter;
      }

      return $data;
    };

    $data = [];

    if (false == $required) {
      $data += ['' => ''];
    }

    $data += Getter::create($items)->setBuilder($callback)->fetch();

    if ($whole_year) {
      $data += [0 => t('Whole year')];
    }

    return $data;
  }

  /** @param array $element */
  protected static function setQuarterDefaultValue(&$element) {
    $map = static::getMonthMap();
    $month = &$element['month'];
    $options = array_flip($month['#options']);
    $index = &$month['#default_value'];

    if ((0 != $index) && isset($map[$index])) {
      $month['#default_value'] = $options[$map[$index]];
      return;
    }
  }

  /**
   * @param $element
   * @param $form_state
   * @param $input
   */
  static function validate_alter(&$element, &$form_state, &$input) {
    $source = &Getter::create($form_state['input'], $element['#parents'])->reference();
    $target = &Getter::create($form_state['values'], $element['#parents'])->reference();

    foreach (array_keys($source) as $index) {
      $input[$index] = $target[$index] = $source[$index];
    }

    foreach (static::getDisableFields() as $field) {
      $input[$field] = 0;
    }

    $map = static::getMonthMap();
    $index = intval($input['month']);

    if ((0 != $index) && isset($map[$index])) {
      $input['month'] = $map[$index];
    }

    if (0 == $index) {
      $field = &Getter::create($form_state['complete form'], $element['#array_parents'])->reference();
      $field['#granularity'] = $element['#granularity'] = ['year'];
    }
  }

}
