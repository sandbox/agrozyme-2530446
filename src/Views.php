<?php

namespace Drupal\date_period;

use Drupal\date_period\Handler\Quarter;
use Drupal\mixin\Caller;
use Drupal\mixin\Getter;
use Drupal\mixin\Traits\Hook;

/** Class Views */
class Views {
  use Hook;

  /** @return array */
  static function hook_views_api() {
    return ['api' => 3];
  }

  /** @param $data */
  static function hook_views_data_alter(&$data) {
    foreach ($data as $table => $items) {
      if (false == is_array($items)) {
        continue;
      }

      foreach ($items as $column => $value) {
        if (empty($value['filter']['is date']) || empty($value['filter']['field_name'])) {
          continue;
        }

        $field = field_info_field($value['filter']['field_name']);
        $period = Getter::create($field, ['settings', 'period'])->setBuilder(Caller::create(null, ''))->fetch(true);

        if ('quarter' == $period) {
          $data[$table][$column]['filter']['handler'] = Quarter::getClassName();
        }

      }
    }

  }

  /** @return array */
  protected static function getHookMap() {
    $class = get_called_class();
    $module = static::getType()->getModule();
    $items = ['views_api', 'views_data_alter'];
    $hooks = [];

    foreach ($items as $item) {
      $hooks[$class]['hook_' . $item] = $module . '_' . $item;
    }

    return $hooks;
  }

}
