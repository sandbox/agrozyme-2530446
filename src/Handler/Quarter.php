<?php

namespace Drupal\date_period\Handler;

use date_views_filter_handler_simple as Base;

/** Class Quarter */
class Quarter extends Base {
  /** @return string */
  static function getClassName() {
    return get_called_class();
  }

  /** @noinspection PhpTooManyParametersInspection
   * @param $form_state
   * @param string $prefix
   * @param string $source
   * @param string $which
   * @param array $operator_values
   * @param array $identifier
   * @param array $relative_id
   *
   * @return mixed
   */
  function date_parts_form(&$form_state, $prefix, $source, $which, $operator_values, $identifier, $relative_id) {
    $form = parent::date_parts_form($form_state, $prefix, $source, $which, $operator_values, $identifier, $relative_id);

    /** @noinspection PhpUndefinedFieldInspection */
    $field = field_info_field($this->definition['field_name']);

    if (empty($form_state['exposed'])) {
      $form[$prefix . '_group'][$prefix]['#field'] = $field;
    } else {
      $form[$prefix]['#field'] = $field;
    }

    return $form;
  }

  /**
   * @param array $form
   * @param array $form_state
   */
  function extra_options_form(&$form, &$form_state) {
    parent::extra_options_form($form, $form_state);
    $granularity = &$form['granularity'];
    $granularity['#default_value'] = $granularity['#value'] = 'month';
    $granularity['#disabled'] = true;
  }

}
