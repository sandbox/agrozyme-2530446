<?php
namespace Drupal\date_period\Date;

use DateInterval;
use DateObject;

/** Class Quarter */
class Quarter {
  const MONTH_PER_QUARTER = 3;
  const QUARTER_PER_YEAR = 4;

  /** @var int $year */
  protected $year;

  /** @var int $quarter */
  protected $quarter;

  /** @var DateObject $start */
  protected $start;

  /** @var DateObject $end */
  protected $end;

  /**
   * Quarter constructor.
   *
   * @param int $year
   * @param int $quarter
   */
  function __construct($year, $quarter) {
    $this->year = $year;
    $this->quarter = $quarter;
  }

  /**
   * @param int $year
   * @param int $month
   *
   * @return static
   */
  static function createFromYearMonth($year, $month) {
    $map = static::getMonthMap();
    $index = intval($month);
    $year = intval($year);
    $item = (0 == $index) ? new static($year, 0) : new static($year, $map[$index]);
    return $item;
  }

  /** @return int[] */
  static function getMonthMap() {
    static $data = [];

    if (false == empty($data)) {
      return $data;
    }

    for ($quarter = 1; $quarter <= static::QUARTER_PER_YEAR; $quarter++) {
      $index = ($quarter - 1) * static::MONTH_PER_QUARTER;

      for ($offset = 1; $offset <= static::MONTH_PER_QUARTER; $offset++) {
        $data[$index + $offset] = $quarter;
      }
    }

    return $data;

  }

  /** @return int[] */
  static function getQuarterMap() {
    static $data = [];

    if (false == empty($data)) {
      return $data;
    }

    for ($quarter = 1; $quarter <= static::QUARTER_PER_YEAR; $quarter++) {
      $index = ($quarter - 1) * static::MONTH_PER_QUARTER;
      $data[$index + 1] = $quarter;
    }

    return $data;
  }

  /** @return int */
  function getYearPart() {
    return $this->year;
  }

  /** @return int */
  function getQuarterPart() {
    return $this->quarter;
  }

  /** @return int */
  function getEndMonth() {
    $quarter = $this->quarter;
    $month = (0 == $quarter) ? 12 : $quarter * static::MONTH_PER_QUARTER;
    return $month;
  }

  /** @return int */
  function getQuarterDays() {
    $days = $this->getStartDate()->diff($this->getEndDate())->days;
    return $days;
  }

  /** @return DateObject */
  function getStartDate() {
    $item = &$this->start;

    if (false == isset($item)) {
      $time = [$this->year, $this->getStartMonth(), 1];
      $item = new DateObject(implode('/', $time));
    }

    return clone $item;
  }

  /** @return int */
  function getStartMonth() {
    $quarter = $this->quarter;
    $month = (0 == $quarter) ? 1 : ($quarter - 1) * static::MONTH_PER_QUARTER + 1;
    return $month;
  }

  /** @return DateObject */
  function getEndDate() {
    $item = &$this->end;

    if (false == isset($item)) {
      $item = $this->addQuarter(1)->getStartDate();
    }

    return clone $item;
  }

  /**
   * @param int $number
   *
   * @return Quarter
   */
  function addQuarter($number) {
    $start = $this->getStartDate();

    if (0 == $this->quarter) {
      $quarter = 4;
      $month = ($quarter - 1) * static::MONTH_PER_QUARTER + 1;
      $time = [$this->year, $month, 1];
      $start = new DateObject(implode('/', $time));
    }

    $date = $start->add($this->getInterval($number));
    $item = static::createFromDate($date);
    return $item;
  }

  /**
   * @param int $number
   *
   * @return DateInterval
   */
  function getInterval($number) {
    $interval = 'P' . ($number * static::MONTH_PER_QUARTER) . 'M';
    $item = new DateInterval($interval);
    return $item;
  }

  /**
   * @param DateObject $date
   *
*@return static
   */
  static function createFromDate(DateObject $date) {
    $map = static::getMonthMap();
    $month = intval($date->format('m'));
    $quarter = $map[$month];
    $item = new static($date->format('Y'), $quarter);
    return $item;
  }

  /**
   * @param $number
   *
   * @return Quarter
   */
  function subQuarter($number) {
    $date = $this->getStartDate()->sub($this->getInterval($number));
    $item = static::createFromDate($date);
    return $item;
  }

}
